"""Stream type classes for tap-omnisend."""

from singer_sdk import typing as th

from tap_omnisend.client import omnisendStream


class ContactsStream(omnisendStream):
    """Define custom stream."""

    name = "contacts"
    path = "/contacts"
    primary_keys = ["contactID"]
    records_jsonpath = "$.contacts[*]"
    replication_key = None

    @property
    def partitions(self):
        partitions = self.config.get("segment_ids")
        if partitions:
            partitions = partitions.split(",")
            partitions = [p.strip() for p in partitions]
            return [{"segment_id": p} for p in partitions]
        return None

    schema = th.PropertiesList(
        th.Property("email", th.StringType),
        th.Property("contactID", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("firstName", th.StringType),
        th.Property("lastName", th.StringType),
        th.Property("country", th.StringType),
        th.Property("countryCode", th.StringType),
        th.Property("state", th.StringType),
        th.Property("city", th.StringType),
        th.Property("postalCode", th.StringType),
        th.Property("address", th.StringType),
        th.Property("gender", th.StringType),
        th.Property("phone", th.ArrayType(th.Property("status", th.StringType))),
        th.Property("phoneNumber", th.StringType),
        th.Property("birthdate", th.DateTimeType),
        th.Property("sent", th.NumberType),
        th.Property("opened", th.NumberType),
        th.Property("clicked", th.NumberType),
        th.Property("status", th.StringType),
        th.Property("lists", th.StringType),
        th.Property("segments", th.ArrayType(th.StringType)),
        th.Property(
            "statuses",
            th.ArrayType(
                th.ObjectType(
                    th.Property("status", th.StringType),
                    th.Property("date", th.DateTimeType),
                )
            ),
        ),
        th.Property(
            "optIns",
            th.ArrayType(
                th.ObjectType(
                    th.Property("date", th.DateTimeType),
                )
            ),
        ),
        th.Property(
            "doubleOptIns",
            th.ArrayType(
                th.ObjectType(
                    th.Property("date", th.DateTimeType),
                )
            ),
        ),
        th.Property("tags", th.ArrayType(th.StringType)),
        th.Property(
            "customProperties",
            th.ObjectType(
                th.Property("externalCreated", th.DateTimeType),
            ),
        ),
        th.Property(
            "identifiers",
            th.ArrayType(
                th.ObjectType(
                    th.Property("id", th.StringType),
                    th.Property("type", th.StringType),
                    th.Property(
                        "channels",
                        th.ObjectType(
                            th.Property(
                                "email",
                                th.ObjectType(
                                    th.Property("status", th.StringType),
                                    th.Property("statusDate", th.DateTimeType),
                                ),
                            )
                        ),
                    ),
                )
            ),
        ),
    ).to_dict()


class CampaignsStream(omnisendStream):
    """Define custom stream."""

    name = "campaigns"
    path = "/campaigns"
    primary_keys = ["campaignID"]
    records_jsonpath = "$.campaign[*]"
    replication_key = None

    schema = th.PropertiesList(
        th.Property("campaignID", th.StringType),
        th.Property("name", th.StringType),
        th.Property("status", th.StringType),
        th.Property("type", th.StringType),
        th.Property("fromName", th.StringType),
        th.Property("subject", th.StringType),
        th.Property("url", th.StringType),
        th.Property("createdAt", th.DateTimeType),
        th.Property("updatedAt", th.DateTimeType),
        th.Property("startDate", th.DateTimeType),
        th.Property("endDate", th.DateTimeType),
        th.Property("sent", th.IntegerType),
        th.Property("clicked", th.IntegerType),
        th.Property("bounced", th.IntegerType),
        th.Property("complained", th.IntegerType),
        th.Property("opened", th.IntegerType),
        th.Property("unsubscribed", th.IntegerType),
        th.Property("allContacts", th.BooleanType),
        th.Property("segments", th.StringType),
        th.Property("excludedSegments", th.StringType),
    ).to_dict()
