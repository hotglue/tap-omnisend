"""REST client handling, including omnisendStream base class."""

from typing import Any, Dict, Optional

import requests
from singer_sdk.authenticators import APIKeyAuthenticator
from singer_sdk.helpers.jsonpath import extract_jsonpath
from singer_sdk.streams import RESTStream


class omnisendStream(RESTStream):
    """omnisend stream class."""

    url_base = "https://api.omnisend.com/v3/"
    _page_size = 250

    @property
    def authenticator(self) -> APIKeyAuthenticator:
        """Return a new authenticator object."""
        return APIKeyAuthenticator.create_for_stream(
            self, key="X-API-KEY", value=self.config.get("api_key"), location="header"
        )

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""
        first_match = next(extract_jsonpath("$.paging", response.json()), None)
        if first_match:
            next_token = first_match.get("next")
            if next_token:
                next_token = next_token.split("?")[-1].split("&")
                return next(t[6:] for t in next_token if "after=" in t)
        return None

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        params["limit"] = self._page_size
        if next_page_token:
            params["after"] = next_page_token
        context = context or {}
        if context.get("segment_id"):
            params["segmentID"] = context.get("segment_id")
        return params
