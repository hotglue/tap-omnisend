"""omnisend tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_omnisend.streams import CampaignsStream, ContactsStream

STREAM_TYPES = [ContactsStream, CampaignsStream]


class Tapomnisend(Tap):
    """omnisend tap class."""

    name = "tap-omnisend"

    config_jsonschema = th.PropertiesList(
        th.Property(
            "api_key",
            th.StringType,
            required=True,
            description="The token to authenticate against the API service",
        ),
        th.Property(
            "start_date",
            th.DateTimeType,
            description="The earliest record date to sync",
        ),
        th.Property(
            "segment_ids",
            th.StringType,
            description="Comma separated segments ids to get",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapomnisend.cli()
